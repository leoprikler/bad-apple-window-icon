(use-modules (sdl2) (sdl2 image) (sdl2 rect) (sdl2 surface)
             (srfi srfi-71))

(sdl-init)

;; The original PV turns out to be 6572 frames and we've rendered them at 64x48
(let ((imgs #s32(62 106))
      (size #s32(64 48))
      (full-size (s32vector 0 0)))
  (array-map! full-size * imgs size)
  (call-with-surface
      (let ((pixels (make-typed-array 'vu8 0 (list 0 (* (array-ref full-size 0)
                                                        (array-ref full-size 1)
                                                        4)))))
         (bytevector->surface pixels
                             (array-ref full-size 0)
                             (array-ref full-size 1)
                             32 (* (array-ref full-size 0) 4)))
    (lambda (surface)
      (for-each
       (lambda (i)
         (let* ((y x (euclidean/ i (array-ref imgs 0)))
                (pos (s32vector x y)))
           (array-map! pos * pos size)
           (format #t ";; ~a ~a~%" i pos)
           ;; This assumes, that you're using ffmpeg to generate bad-apple%d.png
           (call-with-surface (load-image (format #f "bad-apple~a.png" (1+ i)))
             (lambda (img)
               (blit-surface img
                             (make-rect 0 0 (array-ref size 0) (array-ref size 1))
                             surface
                             (make-rect (array-ref pos 0) (array-ref pos 1)
                                        (array-ref size 0) (array-ref size 1)))))))
       (iota (* (array-ref imgs 0) (array-ref imgs 1))))
      (convert-surface-format surface 'index8)
      (save-png surface "bad-apple-indexed.png"))))
